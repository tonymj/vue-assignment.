import Vue from 'vue'
import Vuex from 'vuex'
import Dexie from 'dexie'
import * as firebase from 'firebase'

Vue.use(Vuex)

const db = new Dexie('localStoragePatient')
db.version(1).stores({
  patients: `name, age, bloodGroup`
})

export const store = new Vuex.Store({
  strict: true,
  state: () => ({
    patients: []
  }),
  getters: {
    getPatientsAge0to25: (state) => {
      let age0to25 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age < 26)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age0to25[0]++
            break
          case 'B':
            age0to25[1]++
            break
          case 'AB':
            age0to25[2]++
            break
          case 'O':
            age0to25[3]++
        }
      })
      return age0to25
    },
    getPatientsAge26to35: (state) => {
      let age26to35 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age > 26 && patient.age < 35)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age26to35[0]++
            break
          case 'B':
            age26to35[1]++
            break
          case 'AB':
            age26to35[2]++
            break
          case 'O':
            age26to35[3]++
        }
      })
      return age26to35
    },
    getPatientsAge36to45: (state) => {
      let age36to45 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age > 36 && patient.age < 45)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age36to45[0]++
            break
          case 'B':
            age36to45[1]++
            break
          case 'AB':
            age36to45[2]++
            break
          case 'O':
            age36to45[3]++
        }
      })
      return age36to45
    },
    getPatientsAge46to55: (state) => {
      let age26to35 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age > 46 && patient.age < 55)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age26to35[0]++
            break
          case 'B':
            age26to35[1]++
            break
          case 'AB':
            age26to35[2]++
            break
          case 'O':
            age26to35[3]++
        }
      })
      return age26to35
    },
    getPatientsAge56to65: (state) => {
      let age26to35 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age > 56 && patient.age < 65)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age26to35[0]++
            break
          case 'B':
            age26to35[1]++
            break
          case 'AB':
            age26to35[2]++
            break
          case 'O':
            age26to35[3]++
        }
      })
      return age26to35
    },
    getPatientsAge66to75: (state) => {
      let age26to35 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age > 66 && patient.age < 75)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age26to35[0]++
            break
          case 'B':
            age26to35[1]++
            break
          case 'AB':
            age26to35[2]++
            break
          case 'O':
            age26to35[3]++
        }
      })
      return age26to35
    },
    getPatientsAge76to85: (state) => {
      let age26to35 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age > 76 && patient.age < 85)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age26to35[0]++
            break
          case 'B':
            age26to35[1]++
            break
          case 'AB':
            age26to35[2]++
            break
          case 'O':
            age26to35[3]++
        }
      })
      return age26to35
    },
    getPatientsAge86to95: (state) => {
      let age26to35 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age > 86 && patient.age < 95)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age26to35[0]++
            break
          case 'B':
            age26to35[1]++
            break
          case 'AB':
            age26to35[2]++
            break
          case 'O':
            age26to35[3]++
        }
      })
      return age26to35
    },
    getPatientsAge96to105: (state) => {
      let age26to35 = [0, 0, 0, 0]
      state.patients.filter(patient => (patient.age > 96 && patient.age < 105)).forEach(p => {
        switch (p.bloodGroup) {
          case 'A':
            age26to35[0]++
            break
          case 'B':
            age26to35[1]++
            break
          case 'AB':
            age26to35[2]++
            break
          case 'O':
            age26to35[3]++
        }
      })
      return age26to35
    }
  },
  mutations: {
    setPatient: (state, payload) => {
      state.patients = payload
    }
  },
  actions: {
    populatePatients: ({commit}, payload) => {
      let patients = firebase.database().ref('patients/').push()
      patients.set({
        ...payload
      })
    },
    populateStore: ({commit}) => {
      firebase.database().ref('patients/').orderByChild('name').once('value', function (snapshot) {
        snapshot.forEach((snap) => {
          commit('setPatient', snap.val())
          db.transaction('rw', db.patients, () => {
            for (const o of snap.val) {
              db.patients.add(o)
            }
          }).catch(function (e) {
            console.log(e)
          })
        })
      })
    }
  }
})
