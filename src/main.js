import Vue from 'vue'
import App from './App'
import vuetify from '@/plugins/vuetify'
import HighchartsVue from 'highcharts-vue'
import * as firebase from 'firebase'
import Highcharts from 'highcharts'
import stockInit from 'highcharts/modules/stock'
import mapInit from 'highcharts/modules/map'
import VueFaker from 'vue-faker'
import {store} from '@/store/store'

stockInit(Highcharts)
mapInit(Highcharts)

Vue.use(HighchartsVue)
Vue.use(VueFaker)
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  vuetify,
  store: store,
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyDsNnZ-lKLFyW27bmlsbfLsI4DlXVIfsSc',
      authDomain: 'vue-assignment-api.firebaseapp.com',
      databaseURL: 'https://vue-assignment-api.firebaseio.com',
      projectId: 'vue-assignment-api',
      storageBucket: 'vue-assignment-api.appspot.com'
      // messagingSenderId: '707160600375',
      // appId: '1:707160600375:web:c37b9ed180f277a879f4b2',
      // measurementId: 'G-SX3LPLR9SS'
    })
    this.$store.dispatch('populateStore')
  },
  render: h => h(App)
})
